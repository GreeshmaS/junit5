

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class GreetingImplTest
{
    private  Greeting greeting;

    @BeforeEach
    public void setUp()
    {
        System.out.println("SetUp");
        greeting = new GreetingImpl();
    }
    @Test
    public void greetShouldReturnAValidOutput()
    {
        String result = greeting.greet("JUnit");
        System.out.println("greetShouldReturnAValidOutput");
        assertNotNull(result);
        assertEquals("Hello JUnit",result);
    }
    @Test(expected = IllegalArgumentException.class)
    public void greetShouldThrowAnException_For_Null()
    {
        System.out.println("greetShouldThrowAnException_For_Null");
        greeting.greet(null);
    }
    @Test(expected = IllegalArgumentException.class)
    public void greetShouldThrowAnException_For_An_EmptyString()
    {
        System.out.println("greetShouldThrowAnException_For_An_EmptyString");
        Greeting greeting = new GreetingImpl();
        greeting.greet("");
    }
    @AfterEach
    public void tearDown()
    {
        System.out.println("tearDown");
        greeting = null;
    }


}
